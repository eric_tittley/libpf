# Expected definitions:
#  DEFS
#  INCS_BASE
#  CC, CFLAGS
#  AR, AR_FLAGS

INCS = -I.

OBJS = PF_ReadParameterFile.o \
       PF_RPF_ExtractCommentlessLine.o \
       PF_RPF_ReadArray.o \
       PF_Format.o \
       PF_WriteParameters.o \
       PF_FreeStringArray.o

SRC = $(OBJS:.o=.c)

LIB = libPF.a

all: $(LIB)

$(LIB): $(OBJS)
	$(AR) $(AR_FLAGS) r $(LIB) $(OBJS)

.PHONY: clean
clean:
	-rm *.o
	-rm *~
	-rm splint.log
	cd test; $(MAKE) clean

.PHONY: distclean
distclean: clean
	-rm $(LIB)
	cd test; $(MAKE) distclean

.PHONY: indent
indent:
	indent *.c
	indent *.h

.PHONY: clang-format
clang-format:
	clang-format -i *.c
	clang-format -i *.h
	$(MAKE) -C test clang-format

.PHONY: check
check:
	for file in $(SRC) ; do \
		(splint +posixlib -nullderef -nullpass -realcompare +trytorecover -booltype bool $(INCS) $(DEFS) $$file >> splint.log) \
	done

.PHONY: tests
tests: $(LIB)
	cd test; $(MAKE) CC=$(CC) LD=$(CC)

.SUFFIXES: .o .c

.c.o:
	$(CC) $(CFLAGS) $(DEFS) $(INCS) -c $<
