#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "PF.h"

char *PF_RPF_ExtractCommentlessLine(FILE *const File,
                                    char *const StrippedLine) {
  /* Read a line from the file.
   * Return the line from the beginning up to immediately before the first
   *  comment character.
   * Return NULL if the End of File
   */
  size_t iChar;
  char Line[MAX_LINE_LENGTH];

  char *ErrorStr = fgets(Line, MAX_LINE_LENGTH, File);

  if (ErrorStr == NULL) {
    /* EOF */
    return NULL;
  }

  /* Overwrite leading whitespace for array elements */
  while (Line[0] == ' ') {
    for (iChar = 0; iChar < strlen(Line); iChar++) {
      Line[iChar] = Line[iChar + 1];
    }
  }

  /* Ignore everything after the first comment character */
  char *CommentPoint = strstr(Line, COMMENT_CHARACTER);
  if (CommentPoint != NULL) {
    /* Comment somewhere in the line. Set it's position to the EOL */
    strncpy(CommentPoint, "\0", 1);
  }

  /* Do the same with newline characters */
  CommentPoint = strstr(Line, "\n");
  if (CommentPoint != NULL) {
    strncpy(CommentPoint, "\0", 1);
  }

  /* And again for trailing whitespace (important for strings) */
  for (iChar = strlen(Line) - 1; Line[iChar] == ' '; iChar--) {
    Line[iChar] = '\0';
  }

  /* Copy the entire line, or from the line beginning to the comment character
   */
  strncpy(StrippedLine, Line, MAX_LINE_LENGTH);

  return StrippedLine;
}