/* test1: read in a parameter file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "PF.h"

int main() {
  const int NParameterEntries = 8;
  int ierr;
  int i;
  PF_ParameterEntry *ParameterEntries = NULL;
  FILE *File;

  const char *FileName = "test1.param";

  /* Initialize Parameters with defaults */
  int BooleanParameter = 0;
  int IntegerParameter = 0;
  double DoubleParameter = 0.0;
  char CharacterParameter[MAX_LINE_LENGTH];
  strncpy(CharacterParameter, "default", 8);
  float *FloatArray;
  size_t FloatArrayN;
  char MultiWordParameter[MAX_LINE_LENGTH];
  strncpy(MultiWordParameter, "default", 8);
  char **StringArray;
  size_t StringArrayN;
  char SingleCharParameter = '0';

  /* Allocate memory */
  ParameterEntries = (PF_ParameterEntry *)malloc(sizeof(PF_ParameterEntry) *
                                                 NParameterEntries);

  /* Compile ParameterEntries structure */
  strncpy(ParameterEntries[0].Parameter,
          "BooleanParameter",
          MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[0].Pointer = &BooleanParameter;
  ParameterEntries[0].IsBoolean = 1;
  ParameterEntries[0].IsArray = 0;

  strncpy(ParameterEntries[1].Parameter,
          "IntegerParameter",
          MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[1].Type = INTEGER;
  ParameterEntries[1].Pointer = &IntegerParameter;
  ParameterEntries[1].IsBoolean = 0;
  ParameterEntries[1].IsArray = 0;

  strncpy(ParameterEntries[2].Parameter,
          "DoubleParameter",
          MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[2].Type = DOUBLE;
  ParameterEntries[2].Pointer = &DoubleParameter;
  ParameterEntries[2].IsBoolean = 0;
  ParameterEntries[2].IsArray = 0;

  strncpy(ParameterEntries[3].Parameter,
          "CharacterParameter",
          MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[3].Type = STRING;
  ParameterEntries[3].Pointer = &CharacterParameter;
  ParameterEntries[3].IsBoolean = 0;
  ParameterEntries[3].IsArray = 0;

  strncpy(
      ParameterEntries[4].Parameter, "FloatArray", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[4].Type = FLOAT;
  ParameterEntries[4].Pointer = &FloatArray;
  ParameterEntries[4].IsBoolean = 0;
  ParameterEntries[4].IsArray = 1;
  ParameterEntries[4].NArrayElements = &FloatArrayN;

  strncpy(ParameterEntries[5].Parameter,
          "MultiWordParameter",
          MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[5].Type = STRING;
  ParameterEntries[5].Pointer = &MultiWordParameter;
  ParameterEntries[5].IsBoolean = 0;
  ParameterEntries[5].IsArray = 0;

  strncpy(
      ParameterEntries[6].Parameter, "StringArray", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[6].Type = STRING;
  ParameterEntries[6].Pointer = &StringArray;
  ParameterEntries[6].IsBoolean = 0;
  ParameterEntries[6].IsArray = 1;
  ParameterEntries[6].NArrayElements = &StringArrayN;

  strncpy(ParameterEntries[7].Parameter,
          "SingleCharParameter",
          MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[7].Type = CHAR;
  ParameterEntries[7].Pointer = &SingleCharParameter;
  ParameterEntries[7].IsBoolean = 0;
  ParameterEntries[7].IsArray = 0;

  /* Open parameter file for reading */
  File = fopen(FileName, "r");
  if (File == NULL) {
    printf("%s: %i: ERROR: failed to open file '%s'\n",
           __FILE__,
           __LINE__,
           FileName);
    return EXIT_FAILURE;
  }

  /* Write the parameters */
  ierr = PF_ReadParameterFile(File, ParameterEntries, NParameterEntries);
  if (ierr != EXIT_SUCCESS) {
    printf("%s: %i: ERROR: PF_ReadParameterFile failed\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  /* Close the parameter file */
  fclose(File);

  /* Print the parameters using PF_WriteParameters */
  ierr = PF_WriteParameters(ParameterEntries, NParameterEntries);
  if (ierr != EXIT_SUCCESS) {
    printf("%s: %i: ERROR: PF_WriteParameters failed\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  /* Free the structure */
  free(ParameterEntries);

  /* Free arrays. String arrays need to be done separately because each element
     is malloc()'d individually */
  free(FloatArray);
  PF_FreeStringArray(StringArray, StringArrayN);

  /* Yay! */
  printf("Success!\n");
  return EXIT_SUCCESS;
}
