/***********************************************************************
 *
 * Deallocates an array of N strings. This is necessary because each
 * string was malloc()'d as part of PF_ReadArray()
 *
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "PF.h"

void PF_FreeStringArray(char** ArrayOfStrings, size_t const NStrings) {
  size_t iString;
  for (iString = 0; iString < NStrings; iString++) {
    free(ArrayOfStrings[iString]);
  }
  free(ArrayOfStrings);
}